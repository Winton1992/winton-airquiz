# README #
This README would normally document whatever steps are necessary to get your application up and running.

# What is the product? #
* Project Name: AirQuiz
* Project Overview: 
This project aims to construct an online quiz platform that allows user create and participate the quiz via Internet. It helps users build and participate tests more efficient and flexible, which allows user get rid of the constraining of physical exam paper and time. It also can give the feedback for examinee immediately. It not only alleviates the workload of examiners but also promotes the efficiency of the examination itself.

# What is this repository for? #
* These repository is used for individual part from WEIJIE LI.
* Version Finalize
# Main Functions of AirQuiz web #
* User Profile
* MyQuiz - include View Answer History and Available Quizzes List
* Authentication - Log in, Logout
* Quiz Creation, Edit, Delete and Do
* Automatic generate mark for the result
* Admin management system (WEIJIE LI)
# Dependency of AirQuiz web #
* Java 1.6
* Tomcat 8.0
* Maven 4.0.0
* MySQL

# Who do I talk to? #
* Repo team email: zlai0650@uni.sydney.edu.au

# List of authors #
Weijie Li