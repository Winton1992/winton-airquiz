function getSlogan() {
	$.getJSON("slogan/slogan", function(json) {
		$("p#slogan-content").text(json.content);
		$("p#slogan-author").text(json.author);
	});
}
$(document).ready(function()
	{
		$("a.btn.btn-default").click(getSlogan);
	}	
);
