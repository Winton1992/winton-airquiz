<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
	<head>
    <meta charset="UTF-8">
    <title>AirQuiz</title>
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{% static 'css/dashboard.css' %}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.bootcss.com/bootstrap-contextmenu/0.3.4/bootstrap-contextmenu.min.js"></script>
</head>

	<body data-gr-c-s-loaded="true">
     
    <!-- navigation bar-->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">AirQuiz</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          
                    <li><a href="/elec5619/admin/showAdmin/">Admins</a></li>
					<li><a href="/elec5619/admin/showQuiz/">Quiz</a></li>
					<li><a href="/elec5619/admin/showUser/">Users</a></li>
					<li><a href="/elec5619/admin/showQuestion">Questions</a></li>
					<li><a href="#">Username</a></li>
            <form class="navbar-form navbar-left">
            <input type="text" class="form-control" placeholder="Search...">
            <button class="btn" type="button">Go!</button>
            </form>
            <li><a href="{% url 'dashboard:logout' %}">Logout</a></li>
          </ul>
        </div>
      </div>
    </nav>
        <br></br>
		<h1 align = "center"'>Add new Administrator</h1>
		<form action="add" method="post" align = "center">
			Username: <input type="text" name="username"/>
			<br></br>
			Password:&nbsp <input type="text" name="password"/>
			<br></br>
			<!-- <input type="submit" value="Add"/> -->
           <button type="submit" class="btn btn-primary center-block" value="Add">Add</button>
              
		</form>
	</body>
</html>