<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>home</title>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/Navigation-Clean1.css">
    <link rel="stylesheet" href="resources/css/styles2.css">
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/js/slogan.js"></script>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">AirQuiz </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav">
                    <li class="active" role="presentation"><a href="#">Home </a></li>
                    <li role="presentation"><a href="#">My Profile</a></li>
                    <li role="presentation"><a href="#">My Quiz</a></li>          
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="#">${user.username}</a></li>
                    <li role="presentation"><a href="logout">Logout </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="heading">
        <div class="jumbotron">
            <h1>AirQuiz </h1>
            <p id="slogan-content">${slogan.content}</p>
            <p id="slogan-author">${slogan.author}</p>
            <p><a class="btn btn-default" role="button">Learn more</a></p>
        </div>
    </div>
    <div>
    <form action='/elec5619/quiz/add' method="post">
		<button type="submit" class="btn btn-info btn-sm">Create Quiz</button>
	</form>
	</div>
	
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>