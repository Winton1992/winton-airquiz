<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>profile-edit</title>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/Profile-Edit-Form-Clean.css">
    <link rel="stylesheet" href="resources/css/styles5.css">
</head>

<body>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">AirQuiz </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav">
                    <li role="presentation"><a href="home">Home </a></li>
                    <li class="active" role="presentation"><a href="profile">My Profile</a></li>
                    <li role="presentation"><a href="#">My Quiz</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="#">${user.username}</a></li>
                    <li role="presentation"><a href="logout">Logout </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="form-register">
    
        <form method="post">
            <div class="form-group">
                <h3 class="text-center">${user.username}</h3></div>
            <div class="form-group">
            	<h6 class="text-center">${error}</h6>
            </div>
            <div class="form-group">
                <input class="form-control" type="email" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password_confirm" placeholder="Comfirm Password">
            </div>
            <div class="form-group">
                <button class="btn btn-default form-control" type="submit">Confirm Change</button>
            </div>
        </form>
    </div>
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>