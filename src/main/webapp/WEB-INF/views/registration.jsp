<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>registration</title>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/Register-Form-Clean.css">
    <link rel="stylesheet" href="resources/css/styles1.css">
</head>

<body>

    <div class="form-register">
        <form method="post" action="register" >
            <div class="form-group">
            	<h6 class="text-center">${message}</h6>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="username" placeholder="Username">
            </div>
            <div class="form-group">
                <input class="form-control" type="email" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password_confirm" placeholder="Comfirm Password">
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <div class="radio">
                        <label class="control-label">
                            <input type="radio" name="type" value="teacher">Teacher</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="radio">
                        <label class="control-label">
                            <input type="radio" name="type" value="student"checked>Student</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button class="btn btn-default form-control" type="submit" value="register">Submit Registration</button>
            </div>
            <div class="form-group">
            	<a href="login" class="forgot">I have an account</a>
            </div>
        </form>
    </div>
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>