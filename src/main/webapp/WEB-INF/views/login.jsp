<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/fonts/ionicons.min.css">
    <link rel="stylesheet" href="resources/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="resources/css/styles4.css">
</head>

<body>
			
			
    <div class="login-clean">
        <form method="post" action="j_spring_security_check">
            <h2 class="sr-only">Login Form</h2>
            <div class="illustration"><i class="icon ion-ios-paper"></i></div>
            <div class="form-group">
            	<h6 class="text-center">${msg} ${error}</h6>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" name="username" placeholder="Username">
            </div>
            <div class="form-group">
                <input class="form-control" type="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-block" type="submit" value="login">Log In</button>
            </div><a href="register" class="forgot">Register </a></form>
    </div>
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>