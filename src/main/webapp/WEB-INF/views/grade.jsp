
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
div#test{ border:#000 1px solid; padding:10px 40px 40px 40px; }
</style>
<script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script> <!-- Tether for Bootstrap --> 
<link href="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" rel="stylesheet">
<script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>
</head>
<br>



<body>

<form action='/elec5619/answer/grade/${model.answerOfQuiz_id}' method="get">
<div style="border:1px solid #D3D3D3">
<div style="border:5px solid #FFFFFF">
<label>The Grade of quiz(${model.grade_id}) is:  ${model.grade}/${model.totalQuestions}</label>
</div>
</div>

</form>
<form action='/elec5619/studentHome' method="get">
<br>
<button type="submit" class="btn btn-danger btn-sm">Home</button></form>
</body>