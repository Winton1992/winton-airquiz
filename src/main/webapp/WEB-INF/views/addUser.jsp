<head>
<meta charset="UTF-8">
<title>AirQuiz</title>
<!-- logo for AirQuiz-->
<link rel="icon" type="image/x-icon"
	href="{% static 'images/myCloud_icon.png' %}" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link href="https://fonts.googleapis.com/css?family=Satisfy"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css"
	href="{% static 'css/dashboard.css' %}" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="//cdn.bootcss.com/bootstrap-contextmenu/0.3.4/bootstrap-contextmenu.min.js"></script>


<body data-gr-c-s-loaded="true">

	<!-- navigation bar-->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">AirQuiz</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
                    <li><a href="/elec5619/admin/showAdmin/">Admins</a></li>
					<li><a href="/elec5619/admin/showQuiz/">Quiz</a></li>
					<li><a href="/elec5619/admin/showUser/">Users</a></li>
					<li><a href="#">Username</a></li>
					<form class="navbar-form navbar-left">
						<input type="text" class="form-control" placeholder="Search...">
						<button class="btn" type="button">Go!</button>
					</form>
					<li><a href="{% url 'dashboard:logout' %}">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
</head>
<br></br>
<br> </br>
<body>
<h1 align = "center"'>Create AirQuiz User</h1>

    <div class="form-register" align="center">
    <div class="Question">
       <div class="Container">
        <form  method="post" action="showUser"  >
        
            <div class="form-group">
            	<h6 class="text-center">${message}</h6>
            </div>
            
            <div class="form-group">
                <label class="control-label" for="inputUsername">Username:</label>
                <input  type="text" name="username" placeholder="Username">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            </div>
            
            <div class="form-group">
                <label class="control-label" for="inputEmail">Email:</label>
                <input  type="email" name="email" placeholder="Email">&nbsp
            </div>
            
            <div class="form-group">
                <label class="control-label" for="inputPassword">Password:</label>
                <input  type="password" name="password" placeholder="Password">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            </div>
            
            <div class="form-group">
                <label class="control-label" for="inputPassword">Password Confirm:</label>
                <input type="password" name="password_confirm" placeholder="Comfirm Password">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
            </div>
            
            <div class="form-horizontal" align="center">
                <div >
                    <div class="radio" align="center">
                        <label >
                            <input type="radio" name="type" value="ROLE_TEACHER">Teacher</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    </div>
                     <div class="radio">
                        <label >
                            <input type="radio" name="type" value="ROLE_STUDENT "checked>Student</label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                    </div> 
                </div>
           
             <br></br>
            </div>
         <!--    <div class="form-group">
                <button class="btn btn-primary center-block" type="submit" value="addUser">New User</button>
            </div> -->
            <div class="form-group">
                <!-- <button class="btn btn-default" type="submit" value="register">Submit Registration</button> -->
                <button type="submit" class="btn btn-primary center-block" value="addUser">Submit Registration</button>
            </div>
            <div class="form-group">
            	<a href="login" class="forgot">I have an account</a>
            </div>
            
        </form>
           </div> 
             </div>
    </div>
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>