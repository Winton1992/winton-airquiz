<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
<title>QuizCreation</title>

<script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script> <!-- Tether for Bootstrap --> 
<link href="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" rel="stylesheet">
<script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/elec5619/resources/css/QuizCreation.css">
</head>

<body>
<form action='/elec5619/quiz/add' method="post">
<button type="submit" class="btn btn-info btn-sm">New Quiz</button>
</form>

<div class="container">

<div class="Quiz">
<div class="Container">
<form action='' method="post" >
<div class="page-header"><h3>Quiz Creation</h3></div>
<br>
<div>Title:</div>
<div><input type="text" name="title" class="form-control" placeholder="Title" value="${model.title}" required></div>
<div>Description:</div>
<div><textarea name="description" placeholder="Description" class="form-control" rows="5">${model.description}</textarea></div>
<div class="row" style="padding-top: 10px;padding-left: 20px;padding-right: 20px">
<div  style="float:right">
<a href="/elec5619/quiz/deleteQuiz/${model.quiz_id}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
<button type="submit" class="btn btn-warning btn-sm">Save</button>
<a href="/elec5619/quiz/confirm"><button type="button" class="btn btn-success btn-sm">Confirm</button></a>
</div>
</div>
</form><!-- form end -->

<form action='/elec5619/quiz/edit/${model.quiz_id}/addQuestion' method="post">
<button type="submit" class="btn btn-info btn-sm">New Question</button>
</form>
</div>
</div>
<br>
<c:forEach items="${model.questions}" var="question">
<div class="Question">
<div class="Container">
<form action='/elec5619/quiz/edit/${model.quiz_id}/saveQuestion/${question.id}' method="post">
<!-- <div style="border:1px solid #D3D3D3">
<div style="border:5px solid #FFFFFF"> -->
<div><textarea name="description" placeholder="Question" class="form-control input-sm" rows="3" style="font-size:14px;">${question.description}</textarea></div>
<div class="checkbox" style="padding-top:10px">
<!-- Option table -->
<table>
<tbody>
<tr>

<td>
<c:choose>
<c:when test="${question.option_1_isChecked}">
<input type="checkbox" name="checkbox1" value=1 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox1" value=1>
</c:otherwise>
</c:choose>
</td>
<td>&nbsp;</td>
<td style="width:900"><input name="option_1" type="text" class="form-control" placeholder="option1" value="${question.option_1}" style="font-size:14px;"/></td>
</tr>
<tr>
<td>
<c:choose>
<c:when test="${question.option_2_isChecked}">
<input type="checkbox" name="checkbox2" value=2 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox2" value=2>
</c:otherwise>
</c:choose>
</td>
<td>&nbsp;</td>
<td style="width:900"><input name="option_2" type="text" class="form-control" placeholder="option2" value="${question.option_2}" style="font-size:14px;"/></td>
</tr>
<tr>
<td>
<c:choose>
<c:when test="${question.option_3_isChecked}">
<input type="checkbox" name="checkbox3" value=3 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox3" value=3>
</c:otherwise>
</c:choose>
</td>
<td>&nbsp;</td>
<td style="width:900"><input name="option_3" type="text" class="form-control" placeholder="option3" value="${question.option_3}" style="font-size:14px;"/></td>
</tr>
<tr>
<td>
<c:choose>
<c:when test="${question.option_4_isChecked}">
<input type="checkbox" name="checkbox4" value=4 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox4" value=4>
</c:otherwise>
</c:choose>
</td>
<td>&nbsp;</td>
<td style="width:900"><input name="option_4" type="text" class="form-control" placeholder="option4" value="${question.option_4}" style="font-size:14px;"/></td>
</tr>

</tbody>
</table>

</div>


<!-- control button for question -->
<div style="padding-top: 10px;padding-right:10px;float:right;">
<a href="/elec5619/quiz/edit/${model.quiz_id}/deleteQuestion/${question.id}"><button type="button" class="btn btn-danger btn-sm">Delete</button></a>
<button type="submit" class="btn btn-warning btn-sm">Save</button>
</div>
</form>

</div> <!--Question container end-->
</div> <!--Question end-->
<br>
</c:forEach><!-- forEach end -->

</div><!-- container end -->

<!--  Bootstrap core JavaScript
   ================================================== -->
    <!-- Placed at the end of the document so the pages load faster-->
    <script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
   	<script src="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>
</body>

</html>