<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
<meta charset="UTF-8">
<title>AirQuiz</title>


<!-- logo for AirQuiz-->
<link rel="icon" type="image/x-icon"
	href="{% static 'images/myCloud_icon.png' %}" />

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link href="https://fonts.googleapis.com/css?family=Satisfy"
	rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css"
	href="{% static 'css/dashboard.css' %}" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="//cdn.bootcss.com/bootstrap-contextmenu/0.3.4/bootstrap-contextmenu.min.js"></script>


<body data-gr-c-s-loaded="true">

	<!-- navigation bar-->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">AirQuiz</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
                    <li><a href="/elec5619/admin/showAdmin/">Admins</a></li>
					<li><a href="/elec5619/admin/showQuiz/">Quiz</a></li>
					<li><a href="/elec5619/admin/showUser/">Users</a></li>
					<li><a href="#">Username</a></li>
					<form class="navbar-form navbar-left">
						<input type="text" class="form-control" placeholder="Search...">
						<button class="btn" type="button">Go!</button>
					</form>
					<li><a href="{% url 'dashboard:logout' %}">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>


	<div class="container-fluid">
		<div class="row">
            
			<!--   Table  -->
			<div class="panel-body">
				<table class="table table-hover"">
					<thead>
						<tr>
							<th>QuizID</th>
							<th>Quizname</th>
							<th>Discription</th>
							
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						<!-- {% for assignment in object_list.1 %}  -->
					<br>  </br>
					<c:forEach items="${model.quizs}" var="quiz">
			
							<tr>
								<td>${quiz.id}</td>
								<td>${quiz.title}</td>
							    <td>${quiz.description}</td>
							    
								<td><a href="/elec5619/admin/editQuiz/${quiz.id }">edit</a></td>
								<td><a href="/elec5619/admin/showAdmin/deleteQuiz/${quiz.id }">delete</a></td>  
							</tr>
						</c:forEach> 
						
					</tbody>
				</table>
				</div>
				<%-- <a href="<c:url value="/admin/addQuiz/{id}"/>">Add Quiz</a> --%>
				<form action='/elec5619/quiz/add' method="post">
                   <button type="submit" class="btn btn-primary center-block">New Quiz</button>
                </form>
                </div>
           </div>
			
</body>


      <nav style="text-align: center">
	           <ul class="pagination">
		          <li><a href="#">&laquo;</a></li>
		          <li class="active"><a href="#">1</a></li>
		          <li><a href="#">2</a></li>
		          <li><a href="#">3</a></li>
		          <li><a href="#">4</a></li>
		          <li><a href="#">5</a></li>
		          <li><a href="#">&raquo;</a></li>
	          </ul>
          </nav>
</html>