<%@ include file="/WEB-INF/views/include.jsp"%>
<html>
<head>
<title>QuizAnswerSheet</title>

<script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script> <!-- Tether for Bootstrap --> 
<link href="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" rel="stylesheet">
<script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
<script src="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script>

</head>

<body>
<!-- <a href="/elec5619/answer/start/{id}">
<button type="submit" class="btn btn-danger btn-sm">Start</button></a> -->
<!-- <form action='/elec5619/answer/displayQuiz' method="GET"> -->
<!-- <button type="submit" class="btn btn-info btn-sm">New Quiz</button> -->
<!-- </form> -->

<br>
<div class="container">
<div class="page-header"><h3>QuizName:${model.title}</h3></div>
<br>
<form action='' method="get">
<div>Description:${model.description}</div>
 <div class="row" style="padding-top: 10px;padding-left: 20px;padding-right: 20px">
<div  style="float:right">
<%-- <a href="/elec5619/quiz/deleteQuiz/${model.quiz_id}"></a> --%>
<!-- <button type="submit" class="btn btn-warning btn-sm">Save</button> -->
</div>
</div> 
</form><!-- form end -->


<c:forEach items="${model.answerOfQuestions}" var="answerOfQuestion">
<form action='/elec5619/answer/displayQuiz/${model.answerOfQuiz_id}/saveAnswerOfQuestion/${answerOfQuestion.id}' method="post">
<div style="border:1px solid #D3D3D3">
<div style="border:5px solid #FFFFFF">
<div><label>${answerOfQuestion.description}</label></div>
<div class="checkbox" style="padding-top:10px">
<!-- Option table -->
<table>
<tbody>
<tr>

<%-- <td>
<c:choose>
<c:when test="${question.option_1_isChecked}">
<input type="checkbox" name="checkbox1" value=1 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox1" value=1>
</c:otherwise>
</c:choose>
</td> --%>
<td>&nbsp;</td>
<td style="width:900">
<c:choose>
<c:when test="${answerOfQuestion.option_1_isChecked}">
<label><input type="checkbox" name="checkbox1" id="op1" value="option1" checked>

${answerOfQuestion.option_1}
</label>
</c:when>

<c:otherwise>
<label><input type="checkbox" name="checkbox1" id="op1" value="option1" >
${answerOfQuestion.option_1}
</label>
</c:otherwise>
</c:choose>

</tr>
<tr>
<%-- <td>
<c:choose>
<c:when test="${question.option_2_isChecked}">
<input type="checkbox" name="checkbox2" value=2 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox2" value=2>
</c:otherwise>
</c:choose>
</td> --%>
<td>&nbsp;</td>
<td style="width:900">
<c:choose>
<c:when test="${answerOfQuestion.option_2_isChecked}">
<label><input type="checkbox" name="checkbox2" id="op2" value="option2" checked>
${answerOfQuestion.option_2}
</label>
</c:when>
<c:otherwise>
<label><input type="checkbox" name="checkbox2" id="op2" value="option2" >
${answerOfQuestion.option_2}
</label>
</c:otherwise>
</c:choose>
</tr>
<tr>
<%-- <td>
<c:choose>
<c:when test="${question.option_3_isChecked}">
<input type="checkbox" name="checkbox3" value=3 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox3" value=3>
</c:otherwise>
</c:choose>
</td> --%>
<td>&nbsp;</td>
<td style="width:900">
<c:choose>
<c:when test="${answerOfQuestion.option_3_isChecked}">
<label><input type="checkbox" name="checkbox3" id="op3" value="option3" checked>
${answerOfQuestion.option_3}
</label>
</c:when>
<c:otherwise>
<label><input type="checkbox" name="checkbox3" id="op3" value="option3" >
${answerOfQuestion.option_3}
</label>
</c:otherwise>
</c:choose>
</td>
</tr> 
<tr>
<%-- <td>
<c:choose>
<c:when test="${question.option_4_isChecked}">
<input type="checkbox" name="checkbox4" value=4 checked>
</c:when>
<c:otherwise>
<input type="checkbox" name="checkbox4" value=4>
</c:otherwise>
</c:choose>
</td> --%>
<td>&nbsp;</td>
<td style="width:900">
<c:choose>
<c:when test="${answerOfQuestion.option_4_isChecked}">
<label><input type="checkbox" name="checkbox4" id="op4" value="option4" checked>
${answerOfQuestion.option_4}
</label>
</c:when>
<c:otherwise>
<label><input type="checkbox" name="checkbox4" id="op4" value="option4" >
${answerOfQuestion.option_4}
</label>
</c:otherwise>
</c:choose>
</td>
</tr>

</tbody>
</table>

</div>
</div>
</div>

<!-- control button for question -->
<div style="padding-top: 10px;padding-right:10px;float:right;">
<button type="submit" class="btn btn-success btn-sm">Save</button>

</div>
</form>
<br>
<br>
</c:forEach><!-- forEach end -->
</div><!-- container end -->

 <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!--     <script src="//cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
   	<script src="//cdn.bootcss.com/bootstrap/4.0.0-alpha.4/js/bootstrap.min.js"></script> -->
   	<div  style="float:right">
<form action='/elec5619/answer/cancealAnswerOfQuiz/${model.answerOfQuiz_id}/' method ="post">
<button type="submit" class="btn btn-danger btn-sm">Canceal</button></form>
<form action='/elec5619/answer/submit/${model.answerOfQuiz_id}/' method="post">
<button type="submit" class="btn btn-danger btn-sm">Submit</button></form>
</div>
</body>
</html>


