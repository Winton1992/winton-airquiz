<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{% block title %}MyCould{% endblock %}</title>
    {% load staticfiles %}

    <!-- logo for AirQuiz-->
    <link rel="icon" type="image/x-icon" href="{% static 'images/myCloud_icon.png' %}" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{% static 'css/dashboard.css' %}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdn.bootcss.com/bootstrap-contextmenu/0.3.4/bootstrap-contextmenu.min.js"></script>

    <!-- for right click button-->
    <script src="{% static 'js/bootstrap-contextmenu.js' %}"></script>
    <!-- for dropzone -->
    <link href="{% static 'css/dropzone.css' %}" type="text/css" rel="stylesheet"/>
    <script src="{% static 'js/dropzone.js' %}"></script>
</head>

<body data-gr-c-s-loaded="true">

    <!-- navigation bar-->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">AirQuiz</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Profile</a></li>
            <li><a href="{% url 'dashboard:logout' %}">Logout</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Search...">
          </form>
        </div>
      </div>
    </nav>


    <div class="container-fluid">
      <div class="row">

          <!-- container on the right-->
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="{% url 'dashboard:index' %}">MyDrive<span class="sr-only">(current)</span></a></li>
            <li><a href="{% url 'dashboard:dropzone' %}">New</a></li>
            <li><a href="#">Shared with me</a></li>
            <li><a href="#">Recent</a></li>
            <li><a href="#">Trash</a></li>
          </ul>
        </div>

          <!-- container on the left -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            
        </div>
      </div>
    </div>

</body></html>