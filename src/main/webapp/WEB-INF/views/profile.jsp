<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>profile</title>
    <link rel="stylesheet" href="resources/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resources/css/Navigation-Clean1.css">
    <link rel="stylesheet" href="resources/css/Profile-Form-Clean.css">
    <link rel="stylesheet" href="resources/css/styles3.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">AirQuiz </a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav">
                    <li role="presentation"><a href="home">Home </a></li>
                    <li class="active" role="presentation"><a href="profile">My Profile</a></li>
                    <li role="presentation"><a href="#">My Quiz</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li role="presentation"><a href="#">${user.username}</a></li>
                    <li role="presentation"><a href="logout">Logout </a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="form-profile">
        <form>
            <div class="form-group header">
                <h1 class="text-center">User Profile</h1></div>
            <div class="form-group">
                <div class="col-md-6">
			<!--<div class="form-group">-->
                    <h4 class="text-right">User ID</h4>
                    <h4 class="text-right">User Name </h4>
                    <h4 class="text-right">E-mail </h4></div>
            </div>
            <div class="form-group">
                <div class="col-md-6">
                    <h4 class="text-left">${user.id} </h4>
                    <h4 class="text-left">${user.username} </h4>
                    <h4 class="text-left">${user.email} </h4></div>
            </div>
            <div class="form-group">
				<!--<input class="btn btn-default form-control" href="profileUpdate" type="button">-->
                <a href="profileUpdate"><button class="btn btn-default form-control" href="profileUpdate" type="button">Edit Profile</button></a>
            </div>
			
			<!--<div class="form-group">-->
        </form>
    </div>
    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>