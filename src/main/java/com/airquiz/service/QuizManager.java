package com.airquiz.service;

import java.util.List;

import org.hibernate.Session;

import com.airquiz.domain.Question;
import com.airquiz.domain.Quiz;

public class QuizManager implements QuizManagerInterface{

	private Quiz quiz;
	
	private DatabaseQuizManager databaseQuizManager;
	
	public long getId(){
		return quiz.getId();
	}
	
	public void setTitle(String title){
		quiz.setTitle(title);
	}
	
	public String getTitle(){
		return quiz.getTitle();
	}

	public void setDescription(String description){
		quiz.setDescription(description);
	}
	
	public String getDescription(){
		return quiz.getDescription();
	}
	
	@Override
	public List<Question> getQuestionsByQuizId(long quiz_id){
		return this.databaseQuizManager.getQuestionsByQuizId(quiz_id);
	}
	
	@Override
	public long addQuiz(Quiz quiz) {
		return this.databaseQuizManager.addQuiz(quiz);
	}

	@Override
	public Quiz getQuizById(long id) {
		return this.databaseQuizManager.getQuizById(id);
	}

	@Override
	public void updateQuiz(Quiz quiz) {
		this.databaseQuizManager.updateQuiz(quiz);
	}

	@Override
	public void deleteQuiz(long id) {
		this.databaseQuizManager.deleteQuiz(id);	
	}

	@Override
	public List<Quiz> getQuizsByUserId(long user_id) {
		return this.databaseQuizManager.getQuizsByUserId(user_id);
	}
	
	@Override
	public List<Quiz> getAllQuizs() {
		return this.databaseQuizManager.getAllQuizs();
	}

}
