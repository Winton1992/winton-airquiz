package com.airquiz.service;

import java.io.Serializable;

import java.util.List;

import com.airquiz.domain.Admin;

public interface AdminManagerInterface extends Serializable{
//	for database operation
	public void addAdmin(Admin admin);
	
	public Admin getAdminById(long id);
	
	public void updateAdmin(Admin admin);
	
	public void deleteAdmin(long id);

	public List<Admin> getAllAdmins();

	
}
