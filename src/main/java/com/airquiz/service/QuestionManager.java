package com.airquiz.service;
import java.util.List;
import com.airquiz.domain.Question;
//import com.airquiz.domain.Quiz;

public class QuestionManager implements QuestionManagerInterface {
	
	private Question quiz;
	
	private DatabaseQuestionManager databaseQuestionManager;
	
	@Override
	public void addQuestion(Question question) {
		databaseQuestionManager.addQuestion(question);
	}

	@Override
	public Question getQuestionById(long id) {
		return databaseQuestionManager.getQuestionById(id);
	}

	@Override
	public void updateQuestion(Question question) {
		databaseQuestionManager.updateQuestion(question);
	}

	@Override
	public void deleteQuestion(long id) {
		databaseQuestionManager.deleteQuestion(id);
	}
    
	@Override
	public List<Question> getAllQuestions() {
		return this.databaseQuestionManager.getAllQuestions();
	}
}
