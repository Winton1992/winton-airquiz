package com.airquiz.service;

import java.util.List;

import com.airquiz.domain.Admin;

public class AdminManager implements AdminManagerInterface{

	private Admin admin;
	
	private DatabaseAdminManager databaseAdminManager;
	
	public long getId(){
		return admin.getId();
	}
	
	public void setUsername(String username){
		admin.setUsername(username);
	}
	
	public String getUsername(){
		return admin.getUsername();
	}

	public void setPassword(String password){
		admin.setPassword(password);
	}
	
	public String getPassword(){
		return admin.getPassword();
	}
	
	
	
	@Override
	public void addAdmin(Admin admin) {
		// TODO Auto-generated method stub
		databaseAdminManager.addAdmin(admin);
	}

	@Override
	public Admin getAdminById(long id) {
		// TODO Auto-generated method stub
		return this.databaseAdminManager.getAdminById(id);
	}

	@Override
	public void updateAdmin(Admin admin) {
		// TODO Auto-generated method stub
		this.databaseAdminManager.updateAdmin(admin);
	}

	@Override
	public void deleteAdmin(long id) {
		// TODO Auto-generated method stub
		this.databaseAdminManager.deleteAdmin(id);	
	}
	
	@Override
	public List<Admin> getAllAdmins() {
		return this.databaseAdminManager.getAllAdmins();
	}

}
