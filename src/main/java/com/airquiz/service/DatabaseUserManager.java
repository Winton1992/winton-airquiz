package com.airquiz.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airquiz.domain.User;

@Transactional
@Service(value="userManager")
public class DatabaseUserManager implements UserManagerInterface{
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void addUser(Object user) {
		this.sessionFactory.getCurrentSession().save(user);
	}
	
	@Override
	public User getUserById(long id) {
		return (User) this.sessionFactory.getCurrentSession().get(User.class, id);
	}
	
	@Override
	public User getUserByUsername(String username) {
		String hql_str = "from User user where user.username=:username";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql_str);
		query.setString("username", username);
		User user = (User) query.uniqueResult();
		return user;
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(user);
	}
	
	@Override
	public boolean exists(User user) {
		String hql_str = "from User user where user.username=:username";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql_str);
		query.setString("username", user.getUsername());
		List<User> list = query.list();
		if(list.isEmpty()) return false;
		else return true;
	}
	
	@Override
	public boolean validate(User user) {
		String hql_str = "from User user where user.username=:username and user.password=:password";
		Query query = this.sessionFactory.getCurrentSession().createQuery(hql_str);
		query.setString("username", user.getUsername());
		query.setString("password", user.getPassword());
		List<User> list = query.list();
		if(list.isEmpty()) return false;
		else return true;
	}

	@Override
	public void deleteUser(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		User user = (User)currentSession.get(User.class, id);
		currentSession.delete(user);
	}

	@Override
	public List<User> getUsers() {
		return this.sessionFactory.getCurrentSession().createQuery("FROM User").list();	}

}
