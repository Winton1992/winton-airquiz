package com.airquiz.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.airquiz.domain.Admin;
import com.airquiz.domain.Quiz;

@Service(value="adminManager")
@Transactional

public class DatabaseAdminManager implements AdminManagerInterface{

    private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addAdmin(Admin admin) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(admin);
	}

	@Override
	public Admin getAdminById(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Admin admin = (Admin)currentSession.get(Admin.class, id);
		return admin;
	}

	@Override
	public void updateAdmin(Admin admin) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(admin);
	}

	@Override
	public void deleteAdmin(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Admin admin = (Admin)currentSession.get(Admin.class, id);
		currentSession.delete(admin);
	}

	
	@Override
	public List<Admin> getAllAdmins() {
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from Admin";
		Query query = currentSession.createQuery(hql);
		List<Admin> admins = query.list();
		return admins;
	}


}
