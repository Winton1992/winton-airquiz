package com.airquiz.service;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airquiz.domain.Question;

@Service(value="questionManager")
@Transactional
public class DatabaseQuestionManager implements QuestionManagerInterface {

private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addQuestion(Question question) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(question);
	}

	@Override
	public Question getQuestionById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Question question = (Question)currentSession.get(Question.class, id);
		return question;
	}

	@Override
	public void updateQuestion(Question question) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(question);
	}

	@Override
	public void deleteQuestion(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Question question = (Question)currentSession.get(Question.class, id);
		currentSession.delete(question);
	}

	@Override
	public List<Question> getAllQuestions() {
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from Question";
		Query query = currentSession.createQuery(hql);
		List<Question> questions = query.list();
		return questions;
	}
}
