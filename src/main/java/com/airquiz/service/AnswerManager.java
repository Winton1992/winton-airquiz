package com.airquiz.service;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airquiz.domain.*;

@Service(value="answerManager")
@Transactional
public class AnswerManager implements AnswerManagerInterface {


    private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addAnswerOfQuestion(AnswerOfQuestion answerOfQuestion) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(answerOfQuestion);
	}

	@Override
	public AnswerOfQuestion getAnswerOfQuestionById(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		AnswerOfQuestion answerOfQuestion = (AnswerOfQuestion)currentSession.get(AnswerOfQuestion.class, id);
		return answerOfQuestion;
	}

	@Override
	public void updateAnswerOfQuestion(AnswerOfQuestion answerOfQuestion) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(answerOfQuestion);
	}

	@Override
	public void deleteAnswerOfQuestion(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		AnswerOfQuestion answerOfQuestion = (AnswerOfQuestion)currentSession.get(AnswerOfQuestion.class, id);
		currentSession.delete(answerOfQuestion);
	}
	
	
	
	
	
	
	
	@Override
	public long addAnswerOfQuiz(AnswerOfQuiz answerOfQuiz) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(answerOfQuiz);
		return answerOfQuiz.getId();
	}

	@Override
	public AnswerOfQuiz getAnswerOfQuizById(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		AnswerOfQuiz answerOfQuiz = (AnswerOfQuiz)currentSession.get(AnswerOfQuiz.class, id);
		return answerOfQuiz;
	}

	
	@Override
	public void updateAnswerOfQuiz(AnswerOfQuiz answerOfQuiz) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(answerOfQuiz);
	}

	@Override
	public void deleteAnswerOfQuiz(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		AnswerOfQuiz answerOfQuiz = (AnswerOfQuiz)currentSession.get(AnswerOfQuiz.class, id);
		currentSession.delete(answerOfQuiz);
	}
	
	@Override
	public List<AnswerOfQuestion> getAnswerOfQuestionsByAnswerOfQuizId(long answerOfQuiz_id){
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from AnswerOfQuestion where answerOfQuiz_id=" + answerOfQuiz_id;
		Query query = currentSession.createQuery(hql);
		List<AnswerOfQuestion> answerOfQuestions = query.list();
		return answerOfQuestions;
	}
	
	@Override
	public List<AnswerOfQuiz> getAnswerOfQuizsByUserId(long user_id){
		Session currentSession = this.sessionFactory.getCurrentSession();
//		String hql = "from AnswerOfQuiz where user_id=" + user_id;
		String hqlAnswer = "from AnswerOfQuiz";
		Query queryAnswer = currentSession.createQuery(hqlAnswer);
		List<AnswerOfQuiz> answerOfQuizs = queryAnswer.list();
		return answerOfQuizs;
	}
	@Override
	public List<AnswerOfQuiz> getAllAnswerOfQuizs() {
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from AnserOfQuiz";
		Query query = currentSession.createQuery(hql);
		List<AnswerOfQuiz> answerOfQuizs = query.list();
		return answerOfQuizs;
	}

	

}