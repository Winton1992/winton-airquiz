package com.airquiz.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;

import com.airquiz.domain.Question;
import com.airquiz.domain.Quiz;

public interface QuizManagerInterface extends Serializable {
//	for database operation
	public long addQuiz(Quiz quiz);
	
	public Quiz getQuizById(long id);
	
	public void updateQuiz(Quiz quiz);
	
	public void deleteQuiz(long id);
	
	public List<Question> getQuestionsByQuizId(long quiz_id);
	
	public List<Quiz> getQuizsByUserId(long user_id);
	
	public List<Quiz> getAllQuizs();
	
}
