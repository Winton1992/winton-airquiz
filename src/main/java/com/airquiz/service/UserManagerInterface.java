package com.airquiz.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.airquiz.domain.User;

public interface UserManagerInterface extends Serializable{
//	for database operation
	
	public void addUser(Object user);
	
	public User getUserById(long id) ;
	
	public User getUserByUsername(String username);
	
	public void updateUser(User user);
	
	public boolean exists(User user);
	
	public boolean validate(User user);
	
	public void deleteUser(long id);

	public List<User> getUsers();


}
