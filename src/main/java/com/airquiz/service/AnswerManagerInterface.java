package com.airquiz.service;

import java.io.Serializable;
import java.util.List;

import com.airquiz.domain.*;

public interface AnswerManagerInterface extends Serializable{

    
   
    
  

	public void addAnswerOfQuestion(AnswerOfQuestion answerOfQuestion);
	
	public AnswerOfQuestion getAnswerOfQuestionById(long id);
	
	public void updateAnswerOfQuestion(AnswerOfQuestion answerOfQuestion);
	
	public void deleteAnswerOfQuestion(long id);
	
	
	
	public long addAnswerOfQuiz(AnswerOfQuiz answerOfQuiz);
	
	public AnswerOfQuiz getAnswerOfQuizById(long id);
	
	public void updateAnswerOfQuiz(AnswerOfQuiz answerOfQuiz);
	
	public void deleteAnswerOfQuiz(long id);
	
	public List<AnswerOfQuestion> getAnswerOfQuestionsByAnswerOfQuizId(long answerOfQuiz_id);
	
	public List<AnswerOfQuiz> getAnswerOfQuizsByUserId(long user_id);
	public List<AnswerOfQuiz> getAllAnswerOfQuizs();
    
}