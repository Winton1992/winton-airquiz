package com.airquiz.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airquiz.domain.Slogan;
import com.airquiz.domain.User;

@Transactional
@Service(value="sloganManager")
public class SloganManager {
	@Autowired
	private SessionFactory sessionFactory;
	private long currentSloganId;
	
	public SloganManager() {
		this.currentSloganId = 0;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public void addSlogan(Slogan slogan) {
		this.sessionFactory.getCurrentSession().save(slogan);
	}
	
	public void deleteSloganById(long id) {
		Slogan slogan = (Slogan) this.sessionFactory.getCurrentSession().get(User.class, id);
		this.sessionFactory.getCurrentSession().delete(slogan);
	}

	public Slogan getNextSlogan() {
		this.currentSloganId += 1;
		long nSlogan = (Long) this.sessionFactory.getCurrentSession().createQuery("select count(*) from Slogan").uniqueResult();
		if(this.currentSloganId > nSlogan) {
			this.currentSloganId = 1;
		}
		return (Slogan) this.sessionFactory.getCurrentSession().get(Slogan.class, this.currentSloganId);
	}
}
