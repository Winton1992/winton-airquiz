package com.airquiz.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airquiz.domain.User;

//@Transactional
//@Service(value="userManager")
public class UserManager implements UserManagerInterface{

//	@Autowired
//	private SessionFactory sessionFactory;
//	
    private User user;
	
	private DatabaseUserManager databaseUserManager;

	@Override
	public void addUser(Object user) {
		this.databaseUserManager.addUser(user);
	}
	
//	public void addUser(Object user) {
//	this.sessionFactory.getCurrentSession().save(user);
//}

	@Override
	public User getUserById(long id) {
		return this.databaseUserManager.getUserById(id);
	}

	@Override
	public void updateUser(User user) {
		this.databaseUserManager.updateUser(user);
	}

	@Override
	public void deleteUser(long id) {
		this.databaseUserManager.deleteUser(id);
		
	}

	@Override
	public List<User> getUsers() {
		return this.databaseUserManager.getUsers();
	}

	@Override
	public User getUserByUsername(String username) {
		return this.databaseUserManager.getUserByUsername(username);
	}

	@Override
	public boolean exists(User user) {
		return this.databaseUserManager.exists(user);
	}

	@Override
	public boolean validate(User user) {
		return this.databaseUserManager.validate(user);
	}

	
	
}
