package com.airquiz.service;
import java.util.List;
import java.io.Serializable;

import com.airquiz.domain.Question;;

public interface QuestionManagerInterface extends Serializable {
//	for database operation
	public void addQuestion(Question question);
	
	public Question getQuestionById(long id);
	
	public void updateQuestion(Question question);
	
	public void deleteQuestion(long id);
	
	public List<Question> getAllQuestions();
	
}