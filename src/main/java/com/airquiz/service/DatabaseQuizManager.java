package com.airquiz.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.airquiz.domain.Quiz;
import com.airquiz.domain.Question;

@Service(value="quizManager")
@Transactional
public class DatabaseQuizManager implements QuizManagerInterface{

	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public long addQuiz(Quiz quiz) {
		// TODO Auto-generated method stub
		this.sessionFactory.getCurrentSession().save(quiz);
		return quiz.getId();
	}

	@Override
	public Quiz getQuizById(long id) {
		// TODO Auto-generated method stub
		Session currentSession = this.sessionFactory.getCurrentSession();
		Quiz quiz = (Quiz)currentSession.get(Quiz.class, id);
		return quiz;
	}

	@Override
	public void updateQuiz(Quiz quiz) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		currentSession.merge(quiz);
	}

	@Override
	public void deleteQuiz(long id) {
		Session currentSession = this.sessionFactory.getCurrentSession();
		Quiz quiz = (Quiz)currentSession.get(Quiz.class, id);
		currentSession.delete(quiz);
	}
	
	@Override
	public List<Question> getQuestionsByQuizId(long quiz_id){
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from Question where quiz_id=" + quiz_id;
		Query query = currentSession.createQuery(hql);
		List<Question> questions = query.list();
		return questions;
	}
	
	@Override
	public List<Quiz> getQuizsByUserId(long user_id){
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from Quiz where user_id=" + user_id;
		Query query = currentSession.createQuery(hql);
		List<Quiz> quizs = query.list();
		return quizs;
	}

	@Override
	public List<Quiz> getAllQuizs() {
		Session currentSession = this.sessionFactory.getCurrentSession();
		String hql = "from Quiz";
		Query query = currentSession.createQuery(hql);
		List<Quiz> quizs = query.list();
		return quizs;
	}


}
