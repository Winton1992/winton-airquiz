package com.airquiz.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.airquiz.service.AdminManagerInterface;
import com.airquiz.domain.Admin;
import com.airquiz.service.QuestionManagerInterface;
import com.airquiz.service.QuizManagerInterface;
import com.airquiz.service.UserManagerInterface;
import com.airquiz.service.UserManager;
import com.airquiz.service.UserManagerInterface;
import com.airquiz.service.SloganManager;
import com.airquiz.domain.Slogan;
import com.airquiz.domain.Question;
import com.airquiz.domain.Quiz;
import com.airquiz.domain.Student;
import com.airquiz.domain.Teacher;
import com.airquiz.domain.User;

@Controller
@RequestMapping(value="/admin/**")
public class AdminController {
     
	@Resource(name="quizManager")
	private QuizManagerInterface quizManagerInterface;
	
	@Resource(name="questionManager")
	private QuestionManagerInterface questionManagerInterface;
	
	@Resource(name="adminManager")
	private AdminManagerInterface adminManagerInterface;
    
	@Resource(name="userManager")
	private UserManagerInterface userManagerInterface;
	
	
	@RequestMapping(value="/addAdmin")
	public String addAdmin(Model uiModel) {
		
		return "addAdmin";
	}
	
	//ADD ADMIN
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addAdmin(HttpServletRequest httpServletRequest){

		Admin admin = new Admin();
		admin.setUsername(httpServletRequest.getParameter("username"));
		admin.setPassword(httpServletRequest.getParameter("password"));
		this.adminManagerInterface.addAdmin(admin);
		return "redirect:/admin/showAdmin";
	}
	
	
	//ADD USER
	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public ModelAndView registerGET() {
		return new ModelAndView("addUser");
	}
	
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	public ModelAndView registerPOST(HttpServletRequest request, User user, String password, String password_confirm) {
		System.out.println("showUser");
		
		if(user.getUsername().isEmpty()){
			return new ModelAndView("addUser", "message", "you need a name");
		}
		
		if(user.getPassword().isEmpty()){
			return new ModelAndView("addUser", "message", "your password is empty");
		}
		
		if(!password.equals(password_confirm)){
			return new ModelAndView("addUser", "message", "password does not match");
		}
		
		if(user.getType().equals("student")) {
			System.out.println("is student");
			try{
				userManagerInterface.addUser(new Student(user));
			} catch(Exception e) {
				return new ModelAndView("addUser", "message", "name has been used");
				
			}
			
		} else if(user.getType().equals("teacher")){
			System.out.println("is teacher");
			
			try{
				userManagerInterface.addUser(new Teacher(user));
			} catch(Exception e) {
				return new ModelAndView("addUser", "message", "name has been used");
			}
			
		}
		return new ModelAndView("login");
	}
	
	
	
	 //SHOW USERS
	@RequestMapping(value="/showUser", method=RequestMethod.GET)
	public ModelAndView showUser(HttpServletRequest request,
	HttpServletResponse response) throws Exception, IOException {
        //get quiz data with its questions
		Map<String, Object> model = new HashMap<String, Object>();
		List<User> users = this.userManagerInterface.getUsers();
		model.put("users", users);
		
		return new ModelAndView("showUser","model", model);
	
}
    //ADD NEW USER
	@RequestMapping(value="/showUser", method=RequestMethod.POST)
	public String postUser(HttpServletRequest httpServletRequest){

		User user = new User();
		user.setUsername(httpServletRequest.getParameter("username"));
		user.setEmail(httpServletRequest.getParameter("email"));
		user.setPassword(httpServletRequest.getParameter("password"));
		//user.setEnabled(Integer.valueOf(httpServletRequest.getParameter("enabled")));
		user.setEnabled(1);
		user.setType(httpServletRequest.getParameter("type"));
		this.userManagerInterface.addUser(user);
		return "redirect:/admin/showUser";
	}
	
	
	
	//SHOW QUIZ
	@RequestMapping(value="/showQuiz", method=RequestMethod.GET)
	public ModelAndView showQuiz(HttpServletRequest request,
	HttpServletResponse response) throws Exception, IOException {
//		get quiz data with its questions
		Map<String, Object> model = new HashMap<String, Object>();
		List<Quiz> quizs = this.quizManagerInterface.getAllQuizs();
		model.put("quizs", quizs);
		
		return new ModelAndView("showQuiz","model", model);
	}
	
	//SHOW QUESTIONS
	@RequestMapping(value="/showQuestion", method=RequestMethod.GET)
	public ModelAndView showQuestion(HttpServletRequest request,
	HttpServletResponse response) throws Exception, IOException {
        //get quiz data with its questions
		Map<String, Object> model = new HashMap<String, Object>();
		List<Question> questions = this.questionManagerInterface.getAllQuestions();
		model.put("questions", questions);
		
		return new ModelAndView("showQuestion","model", model);
	
}
	
	//SHOW ADMINS
		@RequestMapping(value="/showAdmin", method=RequestMethod.GET)
		public ModelAndView showAdmin(HttpServletRequest request,
		HttpServletResponse response) throws Exception, IOException {
	        //get quiz data with its questions
			Map<String, Object> model = new HashMap<String, Object>();
			List<Admin> admins = this.adminManagerInterface.getAllAdmins();
			model.put("admins", admins);
			
			return new ModelAndView("showAdmin","model", model);
		
	}
	
		 //DELETE User
		@RequestMapping(value="/deleteUser/{id}", method=RequestMethod.GET)
		public String deleteUser(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
				this.userManagerInterface.deleteUser(id);
				return "redirect:/admin/showUser";
		}
		
        
		
	 
//       //DELETE ADMIN
		@RequestMapping(value="/deleteAdmin/{id}", method=RequestMethod.GET)
		public String deleteAdmin(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
				this.adminManagerInterface.deleteAdmin(id);
				return "redirect:/admin/showAdmin";
		}
	 
        //DELETE QUIZ	
		@RequestMapping(value="/deleteQuiz/{id}", method=RequestMethod.GET)
		public String deleteQuiz(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
//			delete target Quiz and redirect
				this.quizManagerInterface.deleteQuiz(id);
				return "redirect:/admin/showQuiz";
		}

		//EDIT QUIZ
		@RequestMapping(value="/editQuiz/{id}", method=RequestMethod.GET)
		public ModelAndView eidtQuiz(@PathVariable("id") Long id,HttpServletRequest request,
		HttpServletResponse response) throws Exception, IOException {
//			get quiz data with its questions
			Map<String, Object> model = new HashMap<String, Object>();
			Quiz quiz = this.quizManagerInterface.getQuizById(id);
			model.put("title", quiz.getTitle());
			model.put("description", quiz.getDescription());
			List<Question> q = this.quizManagerInterface.getQuestionsByQuizId(id);
			model.put("questions", q);
			model.put("quiz_id", id);
			
			return new ModelAndView("Quiz","model", model);
		}
		
		
}
	
	
	
	