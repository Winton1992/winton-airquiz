package com.airquiz.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.airquiz.domain.Slogan;
import com.airquiz.service.SloganManager;;

@Controller
@RequestMapping("/slogan")
public class SloganController {

	@Autowired
	private SloganManager sloganManager;
	
	@RequestMapping(value = "/slogan", method = RequestMethod.GET)
	public @ResponseBody Slogan sloganGET() {
		
		return this.sloganManager.getNextSlogan();
	}
	
}
