package com.airquiz.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.airquiz.service.UserManager;
import com.airquiz.service.UserManagerInterface;
import com.airquiz.service.SloganManager;
import com.airquiz.domain.Slogan;
import com.airquiz.domain.Student;
import com.airquiz.domain.Teacher;
import com.airquiz.domain.User;

@Controller
public class UserController {
//	@Autowired
	@Resource(name="userManager")
	UserManagerInterface userManager;
//	@Autowired
	@Resource(name="sloganManager")
	SloganManager sloganManager;
	/*
	 * Home Page
	 * */
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		if(user.getType().equals("ROLE_STUDENT")){
			return "redirect:/studentHome";
		} else if(user.getType().equals("ROLE_TEACHER")) {
			return "redirect:/teacherHome";
		} else if(user.getType().equals("ROLE_ADMINISTRATOR")) {
			return "redirect:/administratorHome";
		} else {
			return "redirect:/logout";
		}
	}
	
	/*
	 *  Authentication
	 * */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginGET(@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout) {

	  ModelAndView model = new ModelAndView();
	  if (error != null) {
		model.addObject("error", "Invalid username and password!");
	  }
	  if (logout != null) {
		model.addObject("msg", "You've been logged out successfully.");
	  }
	  model.setViewName("login");
	  return model;
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutGET() {
		return "redirect:j_spring_security_logout";
	}
	
	

	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView error403() {
		return new ModelAndView("/error/403");
	}
	
	
	/*
	 * Registration
	 * */
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public ModelAndView registerGET() {
		return new ModelAndView("registration");
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView registerPOST(HttpServletRequest request, User user, String password, String password_confirm) {
		System.out.println("register");
		
		if(user.getUsername().isEmpty()){
			return new ModelAndView("registration", "message", "you need a name");
		}
		
		if(user.getPassword().isEmpty()){
			return new ModelAndView("registration", "message", "your password is empty");
		}
		
		if(!password.equals(password_confirm)){
			return new ModelAndView("registration", "message", "password does not match");
		}
		
		if(user.getType().equals("student")) {
			System.out.println("is student");
			try{
				userManager.addUser(new Student(user));
			} catch(Exception e) {
				return new ModelAndView("registration", "message", "name has been used");
				
			}
			
		} else if(user.getType().equals("teacher")){
			System.out.println("is teacher");
			
			try{
				userManager.addUser(new Teacher(user));
			} catch(Exception e) {
				return new ModelAndView("registration", "message", "name has been used");
			}
			
		}
		return new ModelAndView("login");
	}
	/*
	 *  Profile
	 * */
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView profileGET() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		return new ModelAndView("profile","user", user);
	}
	
	@RequestMapping(value = "/profileUpdate", method = RequestMethod.GET)
	public ModelAndView profileUpdateGET(@RequestParam(value = "error", required = false) String error) {
		ModelAndView model = new ModelAndView();
		if(error != null) {
			model.addObject("error", "password does not match");
		}
		model.setViewName("profileUpdate");
		return model;
	}
	
	@RequestMapping(value = "/profileUpdate", method = RequestMethod.POST)
	public String profileUpdatePOST(User profile, String password_confirm, String password) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		if(password.equals(password_confirm)){
			System.out.println("confirmed");
			user.setEmail(profile.getEmail());
			user.setPassword(profile.getPassword());
			this.userManager.updateUser(user);
			return "redirect:profile";
		} else {
			return "redirect:profileUpdate?error";
		}
	}
	
	@RequestMapping(value = "/studentHome", method = RequestMethod.GET)
	public ModelAndView studentHome() {
		ModelAndView model = new  ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		Slogan slogan = this.sloganManager.getNextSlogan();
		model.addObject("user", user);
		model.addObject("slogan", slogan);
		model.setViewName("studentHome");
		//return new ModelAndView("studentHome", "user", user, "slogan", slogan);
		return model;
	}
	
	@RequestMapping(value = "/teacherHome", method = RequestMethod.GET)
	public ModelAndView teacherHome() {
		ModelAndView model = new  ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		Slogan slogan = this.sloganManager.getNextSlogan();
		model.addObject("user", user);
		model.addObject("slogan", slogan);
		model.setViewName("teacherHome");
		//return new ModelAndView("teacherHome", "user", user);
		return model;
	}
	
	@RequestMapping(value = "/administratorHome", method = RequestMethod.GET)
	public ModelAndView administratorHome() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		return new ModelAndView("AdministratorHome", "user", user);
	}
	
	@RequestMapping(value = "/userList", method = RequestMethod.GET)
	public ModelAndView userList() {
		List users = (List) this.userManager.getUsers();
		return new ModelAndView("userList", "users", users);
	}
	
}

