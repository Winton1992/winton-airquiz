package com.airquiz.web;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.airquiz.service.QuestionManagerInterface;
import com.airquiz.service.QuizManagerInterface;
import com.airquiz.service.UserManagerInterface;
import com.airquiz.domain.Question;
import com.airquiz.domain.Quiz;
import com.airquiz.domain.User;

@Controller
@RequestMapping(value="/quiz/**")
public class QuizController{
	
	@Resource(name="userManager")
	UserManagerInterface userManager;
	
	@Resource(name="quizManager")
	private QuizManagerInterface quizManagerInterface;
	
	@Resource(name="questionManager")
	private QuestionManagerInterface questionManagerInterface;
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
	public ModelAndView eidtQuiz(@PathVariable("id") Long id,HttpServletRequest request,
	HttpServletResponse response) throws Exception, IOException {
//		get quiz data with its questions
		Map<String, Object> model = new HashMap<String, Object>();
		Quiz quiz = this.quizManagerInterface.getQuizById(id);
		model.put("title", quiz.getTitle());
		model.put("description", quiz.getDescription());
		List<Question> q = this.quizManagerInterface.getQuestionsByQuizId(id);
		model.put("questions", q);
		model.put("quiz_id", id);
		
		return new ModelAndView("Quiz","model", model);
	}
	
	@RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
	public String saveQuiz(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
//	    update quiz to database
			Quiz quiz = this.quizManagerInterface.getQuizById(id);
			quiz.setTitle(httpServletRequest.getParameter("title"));
			quiz.setDescription(httpServletRequest.getParameter("description"));
			this.quizManagerInterface.updateQuiz(quiz);
			return "redirect:/quiz/edit/" + id;
	}
	
	@RequestMapping(value="/deleteQuiz/{id}", method=RequestMethod.GET)
	public String deleteQuiz(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
//		delete target Quiz and redirect
			this.quizManagerInterface.deleteQuiz(id);
			return "redirect:/home";
	}
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addQuiz(HttpServletRequest httpServletRequest){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = this.userManager.getUserByUsername(auth.getName());
		Quiz quiz = new Quiz();
		quiz.setTitle("New Quiz");
		quiz.setUser(user);
		long id = this.quizManagerInterface.addQuiz(quiz);
		return "redirect:/quiz/edit/" + id;
	}
	
	@RequestMapping(value="/edit/{id}/addQuestion", method=RequestMethod.POST)
	public String addQuestion(@PathVariable("id") Long id, HttpServletRequest httpServletRequest){
//    Create new question to database
		Quiz quiz = this.quizManagerInterface.getQuizById(id);
		Question question = new Question();
		question.setQuiz(quiz);
		this.questionManagerInterface.addQuestion(question);
		return "redirect:/quiz/edit/" + id;
	}
	
	@RequestMapping(value="/edit/{id}/deleteQuestion/{question_id}", method=RequestMethod.GET)
	public String addQuestion(@PathVariable("id") Long id, @PathVariable("question_id") Long question_id,HttpServletRequest httpServletRequest){
//    delete question question to database
		this.questionManagerInterface.deleteQuestion(question_id);
		return "redirect:/quiz/edit/" + id;
	}
	
	@RequestMapping(value="/edit/{id}/saveQuestion/{question_id}", method=RequestMethod.POST)
	public String saveQuestion(@PathVariable("id") Long id, @PathVariable("question_id") Long question_id,HttpServletRequest httpServletRequest){
//    update question to database
		Question question = this.questionManagerInterface.getQuestionById(question_id);
		question.setDescription(httpServletRequest.getParameter("description"));
		question.setOption_1(httpServletRequest.getParameter("option_1"));
		question.setOption_2(httpServletRequest.getParameter("option_2"));
		question.setOption_3(httpServletRequest.getParameter("option_3"));
		question.setOption_4(httpServletRequest.getParameter("option_4"));
		
//		check checkbox states
		if(httpServletRequest.getParameter("checkbox1") != null){
			question.setOption_1_isChecked(true);
		} else {
			question.setOption_1_isChecked(false);
		}
		
		if(httpServletRequest.getParameter("checkbox2") != null){
			question.setOption_2_isChecked(true);
		} else {
			question.setOption_2_isChecked(false);
		}
		
		if(httpServletRequest.getParameter("checkbox3") != null){
			question.setOption_3_isChecked(true);
		} else {
			question.setOption_3_isChecked(false);
		}
		
		if(httpServletRequest.getParameter("checkbox4") != null){
			question.setOption_4_isChecked(true);
		} else {
			question.setOption_4_isChecked(false);
		}
		
		this.questionManagerInterface.updateQuestion(question);
		return "redirect:/quiz/edit/" + id;
	}
	
	@RequestMapping(value="/showQuiz", method=RequestMethod.GET)
	public ModelAndView showQuiz(HttpServletRequest request,
	HttpServletResponse response) throws Exception, IOException {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Quiz> quizs = this.quizManagerInterface.getQuizsByUserId(2);
		model.put("quizs", quizs);
		
		return new ModelAndView("showQuiz","model", model);
	}
	
	@RequestMapping(value="/confirm", method=RequestMethod.GET)
	public String confirm(HttpServletRequest request,
	HttpServletResponse response){
		return "redirect:/home";
	}
	
}
