package com.airquiz.web;




import java.util.Map;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletResponse;
import com.airquiz.domain.*;
import com.airquiz.service.*;
import java.util.Set;

@Controller
@RequestMapping(value="/answer/**")
public class AnswerController {
	Map<String,Object> displayModel = new HashMap<String,Object>();
	@Resource(name="answerManager")
	private AnswerManagerInterface answerManagerInterface;
	@Resource(name="quizManager")
//	private QuizManager quizManager;
	private QuizManagerInterface quizManagerInterface;

@RequestMapping(value="/start/{id}",method = RequestMethod.GET)
public String createAnswerOfQuiz(@PathVariable("id") Long id, HttpServletRequest 
		userRequest, HttpServletResponse serviceResponse) throws Exception, IOException
	{


		Quiz quiz = this.quizManagerInterface.getQuizById(id);
		List<Question> qs = this.quizManagerInterface.getQuestionsByQuizId(id);
	    AnswerOfQuiz answerOfQuiz = new AnswerOfQuiz();
		answerOfQuiz.setTitle(quiz.getTitle());
		answerOfQuiz.setDescription(quiz.getDescription());
		long answerOfQuiz_id = answerManagerInterface.addAnswerOfQuiz(answerOfQuiz);
	    answerOfQuiz.setOriginalQuizId(id);
//		List<AnswerOfQuestion> answerOfQuestions = this.answerManagerInterface.getAnswerOfQuestionsByAnswerOfQuizId(answerOfQuiz_id);
       for(int i=0;i<qs.size();i++)
       {
    	   AnswerOfQuestion answerOfQuestion = new AnswerOfQuestion();
   		   answerOfQuestion.setAnswerOfQuiz(answerOfQuiz);
    	  
    	   answerOfQuestion.setDescription(qs.get(i).getDescription());
    	   answerOfQuestion.setOption_1(qs.get(i).getOption_1());
    	   answerOfQuestion.setOption_2(qs.get(i).getOption_2());
    	   answerOfQuestion.setOption_3(qs.get(i).getOption_3());
    	   answerOfQuestion.setOption_4(qs.get(i).getOption_4());
    	  // answerOfQuestions.get(i).setAnswerOfQuiz(answerOfQuiz);
    	   this.answerManagerInterface.updateAnswerOfQuestion(answerOfQuestion);
       }
       
       
	    return "redirect:/answer/displayQuiz/"+answerOfQuiz_id;
	  
		
	}
	
	
	@RequestMapping(value="/displayQuiz/{id}",method = RequestMethod.GET)
     public ModelAndView displayAnswerOfQuiz(@PathVariable("id") Long id, HttpServletRequest 
		userRequest, HttpServletResponse serviceResponse) throws Exception, IOException
{       
		AnswerOfQuiz answerOfQuiz = this.answerManagerInterface.getAnswerOfQuizById(id);
	    Map<String,Object> displayModel = new HashMap<String,Object>();
	    displayModel.put("title", answerOfQuiz.getTitle());
		displayModel.put("description", answerOfQuiz.getDescription());
		List<AnswerOfQuestion> anws = this.answerManagerInterface.getAnswerOfQuestionsByAnswerOfQuizId(id);
		displayModel.put("answerOfQuestions", anws);
		displayModel.put("answerOfQuiz_id",id);
		return new ModelAndView("displayQuiz","model",displayModel);
}
//@RequestMapping(value="/submit/update/{id}", method=RequestMethod.POST)
//public String updateAnswerOfQuiz(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
////    update answer of quiz to database
//	AnswerOfQuiz answerOfQuiz = this.answerManagerInterface.getAnswerOfQuizById(id);
//	answerOfQuiz.setTitle(httpServletRequest.getParameter("title"));
//	answerOfQuiz.setDescription(httpServletRequest.getParameter("description"));
//		this.answerManagerInterface.updateAnswerOfQuiz(answerOfQuiz);
//		return "redirect:/answer/displayQuiz/" + id;
//}
	
@RequestMapping(value="/cancealAnswerOfQuiz/{id}", method=RequestMethod.POST)
public String cancealAnswerOfQuiz(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
     
		this.answerManagerInterface.deleteAnswerOfQuiz(id);
	   return "redirect:/studentHome";
}




@RequestMapping(value="displayQuiz/{id}/saveAnswerOfQuestion/{answerOfQuestion_id}", method=RequestMethod.POST)
public String saveAnswerOfQuestion(@PathVariable("id") Long id, @PathVariable("answerOfQuestion_id") Long answerOfQuestion_id,HttpServletRequest httpServletRequest)
   {

	AnswerOfQuestion answerOfQuestion = this.answerManagerInterface.getAnswerOfQuestionById(answerOfQuestion_id);
	
	if(httpServletRequest.getParameter("checkbox1")!=null)
	{
	   answerOfQuestion.setOption_1_isChecked(true);
	}
	else
	{
	    answerOfQuestion.setOption_1_isChecked(false);
	}
	
	if(httpServletRequest.getParameter("checkbox2")!=null)
	{
	   answerOfQuestion.setOption_2_isChecked(true);
	}
	else
	{
	    answerOfQuestion.setOption_2_isChecked(false);
	}
	if(httpServletRequest.getParameter("checkbox3")!=null)
	{
	   answerOfQuestion.setOption_3_isChecked(true);
	}
	else
	{
	    answerOfQuestion.setOption_3_isChecked(false);
	}
	if(httpServletRequest.getParameter("checkbox4")!=null)
	{
	   answerOfQuestion.setOption_4_isChecked(true);
	}
	else
	{
	    answerOfQuestion.setOption_4_isChecked(false);
	}

    this.answerManagerInterface.updateAnswerOfQuestion(answerOfQuestion);
	return "redirect:/answer/displayQuiz/" + id;
   }

@RequestMapping(value="/submit/{id}", method=RequestMethod.POST)
public String caculateGrade(@PathVariable("id") Long id,HttpServletRequest httpServletRequest){
//    caculate the grade
	AnswerOfQuiz answerOfQuiz = this.answerManagerInterface.getAnswerOfQuizById(id);
	
	List<AnswerOfQuestion> ans = this.answerManagerInterface.getAnswerOfQuestionsByAnswerOfQuizId(id);
	List<Question> ques = this.quizManagerInterface.getQuestionsByQuizId(answerOfQuiz.getOriginalQuizId());
	long count = 0;
	for(int i=0;i<ques.size();i++)
	{
		if((ques.get(i).getOption_1_isChecked() == ans.get(i).getOption_1_isChecked())&&
		(ques.get(i).getOption_2_isChecked() == ans.get(i).getOption_2_isChecked())&&
		(ques.get(i).getOption_3_isChecked() == ans.get(i).getOption_3_isChecked())&&
		(ques.get(i).getOption_4_isChecked() == ans.get(i).getOption_4_isChecked()))
		{
			 count ++;
		}
	}
	System.out.println(count);
	answerOfQuiz.setGrade(count);
	this.answerManagerInterface.updateAnswerOfQuiz(answerOfQuiz);
	return "redirect:/answer/grade/" + id;
}
@RequestMapping(value="/grade/{id}",method = RequestMethod.GET)
public ModelAndView displayGrade(@PathVariable("id") Long id, HttpServletRequest 
	userRequest, HttpServletResponse serviceResponse) throws Exception, IOException
{       
	
	
	AnswerOfQuiz answerOfQuiz = this.answerManagerInterface.getAnswerOfQuizById(id);
   List<AnswerOfQuestion> a = this.answerManagerInterface.getAnswerOfQuestionsByAnswerOfQuizId(id);
   Map<String,Object> displayModel = new HashMap<String,Object>();
   displayModel.put("grade", answerOfQuiz.getGrade());
   displayModel.put("grade_id",answerOfQuiz.getOriginalQuizId());
   displayModel.put("totalQuestions", a.size());
  return new ModelAndView("grade","model",displayModel);


}
//@RequestMapping(value="turn",method = RequestMethod.POST)
//public String getBackHome(HttpServletRequest httpServletRequest)
//{
//	  return "redirect:/answer/start/14";
//}
}
