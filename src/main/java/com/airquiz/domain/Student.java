package com.airquiz.domain;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="user")
@DiscriminatorValue("S")
public class Student extends User{
	
	public Student(){}

	public Student(User user) {
		this.setId(user.getId());
		this.setEmail(user.getEmail());
		this.setPassword(user.getPassword());
		this.setUsername(user.getUsername());
		this.setType("ROLE_STUDENT");
		this.setEnabled(1);
	}
	
}