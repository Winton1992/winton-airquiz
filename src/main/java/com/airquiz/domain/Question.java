package com.airquiz.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="Question")
public class Question implements Serializable 
{
	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	@Column(name="Description")
	private String description;
	
    public void setDescription(String description)
    {
    	this.description = description;
    }
    
    public String getDescription()
    {
    	return description;
    }
    
    @Column(name="Option_1")
	private String option_1;
	
    public void setOption_1(String option)
    {
    	this.option_1 = option;
    }
    
    public String getOption_1()
    {
    	return option_1;
    }
    
    @Column(name="Option_2")
	private String option_2;
	
    public void setOption_2(String option)
    {
    	this.option_2 = option;
    }
    
    public String getOption_2()
    {
    	return option_2;
    }
    
    @Column(name="Option_3")
	private String option_3;
	
    public void setOption_3(String option)
    {
    	this.option_3 = option;
    }
    
    public String getOption_3()
    {
    	return option_3;
    }
    
    @Column(name="Option_4")
	private String option_4;
	
    public void setOption_4(String option)
    {
    	this.option_4 = option;
    }
    
    public String getOption_4()
    {
    	return option_4;
    }
    
    @Column(name="Option_1_isChecked", columnDefinition="BOOLEAN default false")
	private boolean option_1_isChecked;
	
    public void setOption_1_isChecked(boolean flag)
    {
    	this.option_1_isChecked = flag;
    }
    
    public boolean getOption_1_isChecked()
    {
    	return option_1_isChecked;
    }
    
    @Column(name="Option_2_isChecked", columnDefinition="BOOLEAN default false")
	private boolean option_2_isChecked;
	
    public void setOption_2_isChecked(boolean flag)
    {
    	this.option_2_isChecked = flag;
    }
    
    public boolean getOption_2_isChecked()
    {
    	return option_2_isChecked;
    }
    
    @Column(name="Option_3_isChecked", columnDefinition="BOOLEAN default false")
	private boolean option_3_isChecked;
	
    public void setOption_3_isChecked(boolean flag)
    {
    	this.option_3_isChecked = flag;
    }
    
    public boolean getOption_3_isChecked()
    {
    	return option_3_isChecked;
    }
    
    @Column(name="Option_4_isChecked", columnDefinition="BOOLEAN default false")
	private boolean option_4_isChecked;
	
    public void setOption_4_isChecked(boolean flag)
    {
    	this.option_4_isChecked = flag;
    }
    
    public boolean getOption_4_isChecked()
    {
    	return option_4_isChecked;
    }
    
    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH }, optional = true)
    @JoinColumn(name="quiz_id")
    private Quiz quiz;
    
    public Quiz getQuiz(){
    	return quiz;
    }
    
    public void setQuiz(Quiz quiz){
    	this.quiz = quiz;
    }
    
}
