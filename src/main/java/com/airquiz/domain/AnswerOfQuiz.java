package com.airquiz.domain;

import java.util.List;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="AnswerOfQuiz")
public class AnswerOfQuiz implements Serializable 
{	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	@Column(name="Title")
	private String title;
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	@Column(name="Description")
	private String description;
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	@Column(name="OriginalQuizId")
	private long originalQuizId;
	
	public long getOriginalQuizId()
	{
		return originalQuizId;
	}
	
	public void setOriginalQuizId(long originalQuizId)
	{
		this.originalQuizId = originalQuizId;
	}
	
	@Column(name="Grade")
	private long grade;
	
	public long getGrade()
	{
		return grade;
	}
	
	public void setGrade(long grade)
	{
		this.grade = grade;
	}
	
	
	@OneToMany(cascade = { CascadeType.REFRESH, CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE },
			mappedBy ="answerOfQuiz")
	private Set<AnswerOfQuestion> answerOfQuestions = new HashSet<AnswerOfQuestion>();
	
	public Set<AnswerOfQuestion> getAnswerOfQuestions(){
		return answerOfQuestions;
	}
	
	public void setAnswerOfQuestions(Set<AnswerOfQuestion> answerOfQuestions){
		this.answerOfQuestions = answerOfQuestions;
	}
	
	public void addAnswerOfQuestion(AnswerOfQuestion answerOfQuestion){
		answerOfQuestion.setAnswerOfQuiz(this);
		this.answerOfQuestions.add(answerOfQuestion);
	}
	
	@ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH }, optional = true)
    @JoinColumn(name="user_id")
    private User user;
    
    public User getUser(){
    	return user;
    }
    
    public void setUser(User user){
    	this.user = user;
    }
	
	
}
