package com.airquiz.domain;

import java.util.List;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Quiz")
public class Quiz implements Serializable 
{	
	@Id
	@GeneratedValue
	@Column(name="Id")
	private long id;
	
	public long getId()
	{
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	@Column(name="Title")
	private String title;
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	@Column(name="Description")
	private String description;
	
	public String getDescription()
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	@OneToMany(cascade = { CascadeType.REFRESH, CascadeType.PERSIST,CascadeType.MERGE, CascadeType.REMOVE },mappedBy ="quiz")
	private Set<Question> questions = new HashSet<Question>();
	
	public Set<Question> getQuestions(){
		return questions;
	}
	
	public void setQuestions(Set<Question> questions){
		this.questions = questions;
	}
	
	public void addQuestion(Question question){
		question.setQuiz(this);
		this.questions.add(question);
	}
	
	@ManyToOne(cascade = {CascadeType.MERGE,CascadeType.REFRESH }, optional = true)
    @JoinColumn(name="user_id")
    private User user;
    
    public User getUser(){
    	return user;
    }
    
    public void setUser(User user){
    	this.user = user;
    }
	
	
}
