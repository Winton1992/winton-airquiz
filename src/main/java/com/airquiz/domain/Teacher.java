package com.airquiz.domain;


import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="user")
@DiscriminatorValue("T")
public class Teacher extends User {

	public Teacher(){}
	
	public Teacher(User user) {
		this.setId(user.getId());
		this.setEmail(user.getEmail());
		this.setPassword(user.getPassword());
		this.setUsername(user.getUsername());
		this.setType("ROLE_TEACHER");
		this.setEnabled(1);
	}
	
}